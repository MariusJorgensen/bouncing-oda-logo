let speed = 20;
let scale = 0.07; // Image scale (I work on 1080p monitor)
let canvas;
let ctx;
let selectColor = 0;
let odaColors = [
    'rgb('+255+','+245+', '+230+')',
    'rgb('+250+','+60+', '+15+')',
    'rgb('+40+','+15+', '+60+')',
    'rgb('+165+','+115+', '+210+')'
];

let oda = {
    x: 200,
    y: 300,
    xspeed: 10,
    yspeed: 10,
    img: new Image()
};

(function main(){
    canvas = document.getElementById("tv-screen");
    ctx = canvas.getContext("2d");
    oda.img.src = 'oda-logo-rs.png';

    //Draw the "tv screen"
    canvas.width  = window.innerWidth;
    canvas.height = window.innerHeight;

    pickColor();
    update();
})();

function update() {
    setTimeout(() => {
        //Draw the canvas background
        ctx.fillStyle = '#000';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        //Draw Oda Logo and his background
        ctx.fillStyle = logoColor;
        ctx.fillRect(oda.x, oda.y, oda.img.width*scale, oda.img.height*scale);
        ctx.drawImage(oda.img, oda.x, oda.y, oda.img.width*scale, oda.img.height*scale);
        //Move the logo
        oda.x+=oda.xspeed;
        oda.y+=oda.yspeed;
        //Check for collision 
        checkHitBox();
        update();   
    }, speed)
}

//Check for border collision
function checkHitBox(){
    if(oda.x+oda.img.width*scale >= canvas.width || oda.x <= 0){
        oda.xspeed *= -1;
        pickColor();
    }
        
    if(oda.y+oda.img.height*scale >= canvas.height || oda.y <= 0){
        oda.yspeed *= -1;
        pickColor();
    }    
}

//Pick a random color in RGB format
function pickColor(){
    logoColor = odaColors[selectColor];

    if (selectColor == 3) {
        selectColor = 0;
    } else {
        selectColor++;
    }
}